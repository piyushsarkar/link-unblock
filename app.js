const express = require('express');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'))
app.use(express.json({extended : false}));

// Route
app.use('/api', require('./routes/api'));
app.use('/', require('./routes/index'));
app.all('*', (req, res) => {
    res.render('404');
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));