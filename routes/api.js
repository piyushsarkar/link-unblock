const express = require("express");
const router = express.Router();

router.all("/", async (req, res) => {
  try {
    // recieving query and body parameters
    const query = req.query;
    const body = req.body;
    // combining both(query and body parameters) into one
    const input = { ...query, ...body };
    let main_url = input.url;
    console.log(main_url);
    main_url = main_url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "");
    main_url = Buffer.from(main_url, "ascii").toString("hex");
    const unblockedLink =
      req.protocol + "://" + req.headers.host + "/r/" + main_url;

    res.status(201).json({
      status: "success",
      data: unblockedLink,
    });
  } catch (err) {
    console.error(err);
    res.status(400).json({
      status: "error",
      data: "somthing went wrong"
    });
  }
});

module.exports = router;
