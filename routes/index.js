const express = require("express");
const router = express.Router();
const metaExtract = require("meta-extractor");

/****  routes *****/
//HOME ROUTE
router.get("/", async (req, res) => {
  res.render("index");
});

//DECODE AND REDIRECT ROUTE
router.get("/r/:r", async (req, res) => {
  try {
    const decodedUrl = Buffer.from(req.params.r, 'hex').toString('ascii')
    const fullUrl = "http://" + decodedUrl;

    // finding referer and user-agent
    let refer = req.headers.referer;
    let user_agent = req.headers["user-agent"];
    if (refer == undefined) refer = "unknown";
    if (user_agent.match(/.*facebook.*/)) var fb = true; 
    console.log(`Referer link: ${refer} \nUser-agent: ${user_agent}\n`);

    // if referer is facebook show diffrent page else redirect.
    if (refer.match(/.*facebook.*/) || user_agent.match(/.*facebook.*/) || user_agent.match(/.*iPhone.*Mac.*/)) {
      // fetching meta-tags from website.
      const metaTag = await metaExtract({ uri: fullUrl });
      res.render("facebook", {
        fb : fb,
        url: fullUrl,
        metaTag: metaTag,
      });
    } else {
      res.redirect(fullUrl);
    }
  } catch (err) {
    console.error(err);
    res.render("500");
  }
});

module.exports = router;
